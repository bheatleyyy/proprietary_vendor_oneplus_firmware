LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),oneplus8)

RADIO_FILES := $(wildcard $(LOCAL_PATH)/radio/*)
$(foreach f, $(notdir $(RADIO_FILES)), \
    $(call add-radio-file,radio/$(f)))

FIRMWARE_IMAGES := \
    abl \
    aop \
    bluetooth \
    cmnlib \
    cmnlib64 \
    devcfg \
    dsp \
    featenabler \
    hyp \
    imagefv \
    keymaster \
    logo \
    modem \
    multiimgoem \
    qupfw \
    storsec \
    tz \
    uefisecapp \
    xbl \
    xbl_config


AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)

endif
